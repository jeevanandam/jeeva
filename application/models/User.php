<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
	
	public function signup($data)
	{
		$result=$this->db->insert('users',$data);
		 return $this->db->insert_id();
    }
	public function emailexist($email)
	{
		$this->db->where('email',$email);
		return $this->db->count_all_results('users');	   
	}
	
	public function login($email,$password)
	{
	    $this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('email',$email);
		$this -> db -> where('password', md5(md5($password)));
		$query = $this -> db -> get();
		if($query -> num_rows() == 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	public function getusers($insert)
	{
		$this->db->select('*');
        $this ->db->from('users');
		$this ->db->where('user_id',$insert);
        $result = $this->db->get();
		return $result->result();	   
    }
    
	
}