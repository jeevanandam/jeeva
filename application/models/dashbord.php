<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashbord extends CI_Model {
	
	public function insertimage($data)
	{
		$result=$this->db->insert('images',$data);
    }
	
	public function getprofiles()
	{
		$userid= $this->session->userdata('userId'); 
		$this->db->select('img_url');
		$this->db->from('images');
		$this->db->where('user_id',$userid);
		$result = $this->db->get();
		
		return $result->result();	 
    }
}