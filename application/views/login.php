<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/login/css/normalize.css">

    
        <link rel="stylesheet" href="<?php echo base_url()?>assets/login/css/style.css">

    
  </head>

  <body>

    <div class="form">
      
      <ul class="tab-group">
        <li class="tab"><a href="#signup">Sign Up</a></li>
        <li class="tab active"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="<?php echo base_url()?>welcome/signup" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                <span class="req"></span>
              </label>
              <input type="text" name="firstname" required autocomplete="off" placeholder="First Name *" />
            </div>
        
            <div class="field-wrap">
              <label>
                <span class="req"></span>
              </label>
              <input type="text" name="lastname"required autocomplete="off" placeholder="Last Name *"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              <span class="req"></span>
            </label>
            <input type="email" id="emailid" name="emailid" required autocomplete="off" placeholder="Email Address *"/>
             <span id="emailconfirm" class="emailconfirm"></span>
          </div>
          
          <div class="field-wrap">
            <label>
              <span class="req"></span>
            </label>
            <input name="password" placeholder="Set A Password *" id="password"type="password"required autocomplete="off"/>
          </div>

          <div class="field-wrap">
            <label>
              <span class="req"></span>
            </label>
            <input name="confirmpassword" id="confirmpassword" type="password"  placeholder="Set A Confirm Password *" onkeyup="checkPass(); return false;" required autocomplete="off"/>
             <span id="confirmMessage" class="confirmMessage"></span>
          </div>
          
          <button type="submit" id="signupsubmit" class="button button-block"/>Sign Up</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Welcome Login!</h1>
          
          <form action="<?php echo base_url()?>welcome/login" method="post">
          
            <div class="field-wrap">
            <label>
              <span class="req"></span>
            </label>
            <input type="email"  name="email" placeholder="Email Address *" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              <span class="req"></span>
            </label>
            <input type="password" name="password" placeholder="Password *" required autocomplete="off"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block"/>Log In</button>
          
          </form>

        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
    

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="<?php echo base_url()?>assets/login/js/index.js"></script>
    <script>
    function checkPass()
      {
            var pass1 = document.getElementById('password');
            var pass2 = document.getElementById('confirmpassword');
            
            var message = document.getElementById('confirmMessage');
           
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
           
            if(pass1.value == pass2.value){
               // pass2.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Passwords Match!"
            }else{
              // pass2.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Passwords Do Not Match!"
            }
        } 
    </script>
    <script >
    $( document ).ready(function() {  
   $('#confirmpassword').click (function() {

      var email = $("#emailid").val();
      $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>welcome/existemail/",
              async: false,
              data:  'emailid='+ email,
              dataType: 'html',
              success:function(data){
                if(data == '1')
               {
                var message = document.getElementById('emailconfirm');
                var badColor = "#ff6666";
                message.style.color = badColor;
                message.innerHTML = "Email Alerady Exist!";
				}
				else{
					var message = document.getElementById('emailconfirm');
					var badColor = "white";
					message.style.color = badColor;
					message.innerHTML = "";
				}				},              
            });

         });
      
       });
    </script>
  </body>
</html>
