

<!DOCTYPE html>
<html>
<head>
	<title>Jeeva Friend's Images</title>
	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
</head>
<body>
	<script>
	 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-30027142-1', 'w3layouts.com');
		 ga('send', 'pageview');
	</script>
<script async type='text/javascript' src='//cdn.fancybar.net/ac/fancybar.js?zoneid=1502&serve=C6ADVKE&placement=w3layouts' id='_fancybar_js'></script>


	<div class="header">
		<div class="header-left header-contact">
			<div class="logo">
				<a href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png" alt=""></a>
			</div>
			<div class="top-nav">
				<ul >
					<li><a href="<?php echo base_url();?>welcome/index" >HOME</a></li>
					<li><a href="#" class="black1"> ABOUT ME</a></li>
                    <li  class="active" ><a href="<?php echo base_url();?>upload/index" class="black1"> UPLOAD</a></li>
					<li><a href="#" class="black3" > SERVICES</a></li>
					<li><a href="#" class="black4" > CONTACT</a></li>
				</ul>
				<!-- <ul >
					<li><a href="https://p.w3layouts.com/demos/kappe/web/index.html" >HOME</a></li>
					<li><a href="https://p.w3layouts.com/demos/kappe/web/about.html" class="black1"> ABOUT ME</a></li>
                    <li  class="active" ><a href="<?php echo base_url();?>upload/index" class="black1"> UPLOAD</a></li>
					<li><a href="https://p.w3layouts.com/demos/kappe/web/404.html" class="black3" > SERVICES</a></li>
					<li><a href="https://p.w3layouts.com/demos/kappe/web/contact.html" class="black4" > CONTACT</a></li>
				</ul> -->
			</div>
			<ul class="social-in">
				<li><a href="#"><i> </i></a></li>
				<li><a href="#"><i class="gmail"> </i></a></li>
				<li><a href="#"><i class="twitter"> </i></a></li>
				<li><a href="#"><i class="pin"> </i></a></li>
				<li><a href="#"><i class="dribble"> </i></a></li>
				<li><a href="#"><i class="behance"> </i></a></li>
			</ul>
			<p class="footer-class">Copyright &copy; 2015 Kappe. All rights  Reserved |  Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
		</div>
		<!---->
		<div class="header-top">
			<div class="logo-in">
				<a href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png" alt=""></a>
			</div>
			<div class="top-nav-in">
			<span class="menu"><img src="<?php echo base_url();?>assets/images/menu.png" alt=""> </span>
				<ul >
					<li><a href="<?php echo base_url();?>welcome/index" >HOME</a></li>
					<li><a href="#" class="black1"> ABOUT ME</a></li>
                    <li  class="active" ><a href="<?php echo base_url();?>upload/index" class="black1"> UPLOAD</a></li>
					<li><a href="#" class="black3" > SERVICES</a></li>
					<li><a href="#" class="black4" > CONTACT</a></li>
				</ul>
				<script>
					$("span.menu").click(function(){
						$(".top-nav-in ul").slideToggle(500, function(){
						});
					});
			</script>

			</div>
			<div class="clear"> </div>
		</div>
			<!---->
		<div class="content">
			<div class="contact">
			
				<form action="<?php echo base_url() ?>upload/index" method="post" enctype="multipart/form-data">
					<div class="top-contact">
						<h3>Upload Your Image</h3>
						<div class="grid-contact">
							<div class="your-top">
								<input type="file" name="uploadimage" value="Image Name" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Image Name';}">								
								<div class="clear"> </div>
							</div>
						</div>
						<div class="grid-contact-in">
							<input type="submit" value="UPLOAD">
						</div>
						<div class="clear"> </div>
					</div>
				</form>
			</div>
			<div class="map">
			<iframe src="https://www.google.com/maps/embed/v1/place?q=Vellore,+Tamil+Nadu,+India&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
			</div>
			
		</div>

		<div class="clear"> </div>
	</div>
</body>
</html>