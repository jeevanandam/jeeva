<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url','html'));
		$this->load->model('dashbord','',TRUE);
		$this->load->database();
		$this->load->library('email');
	}

	public function index()
	{
		
		$this->load->view('index');
	}
	
	public function home()
	{ 
		$data['getprofiles']=$this->dashbord->getprofiles();
		$this->load->view('index',$data);
		
	}
	
}