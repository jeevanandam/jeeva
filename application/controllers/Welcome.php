<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url','html'));
		$this->load->model('user','',TRUE);
		$this->load->database();
		$this->load->library('email');
	}

	public function index()
	{
		if($this->session->userdata('email')){
				redirect('/dashboard/home/');
		}else
		{
		$this->load->view('login');
		}
		
	}
	
	public function existemail()
	{
		$email=$_POST['emailid'];
		$emailexist= $this->user->emailexist($email);
		print_r($emailexist);
    }
	
	public function signup()
	{
		if($_POST){
			$firstname=$this->input->post('firstname');
			$lastname=$this->input->post('lastname');
			$email=$this->input->post('emailid');
			$password=$this->input->post('password');
			$data=array(
				'firstname'=>$firstname,
				'lastname'=>$lastname,
				'email'=>$email,
				'password'=>md5(md5($password))
			);
			$insert= $this->user->signup($data);
			$getdetails= $this->user->getusers($insert);
			foreach($getdetails as $key){
				$userId=$key->user_id;
				$firstname=$key->firstname;
				$lastname=$key->lastname;
				$email=$key->email;
			}
			$datas=array(
			'userId'=>$userId,
				'firstname'=>$firstname,
				'lastname'=>$lastname,
				'email'=>$email
			);
			$this->session->set_userdata($datas);
			redirect('dashboard/home');
		}
		redirect('welcome/index');
	}
	
	public function login()
	{ 
		if($_POST){
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$emailexist = $this->user->emailexist($email);
			
			if($emailexist == 0){ 	
				$email_error="EmailID Is Not Avaliable !!"; 
				print_r($email_error);die;				
				//$this->session->set_flashdata('msg', $email_error);	
				//redirect('welcome/index');
			}else{  
				$login = $this->user->login($email,$password);
				
				if($login)
				{ 	
					$userId = $login[0]->user_id;
					$email = $login[0]->email;
					$firstname = $login[0]->firstname;
					$lastname = $login[0]->lastname;
					
					$msg="User Successfully Logined !!";
					$data = array(
						'Error' => 'False',
						'Message' => $msg,
						'email' => $email,
						'is_logged_in' => true,
						'userId' => $userId,
						'firstname' => $firstname,
						'lastname' =>$lastname
					);
					
					$this->session->set_userdata($data);
					redirect('dashboard/home');
				}else{
					$password_error="Password Is Not Matching !!.";               
					//$this->session->set_flashdata('msg', $password_error);	
					//redirect('welcome/index');
					print_r($password_error);die;
				}
				$this->session->set_userdata($data);
				redirect('dashboard/home');
			}
		}		
	}
	
	public function logout()
	{ 
		$this->session->sess_destroy();
        redirect('welcome/index');
	}
}
